function Basket=changeAname(Basket,type)
if type==2
Basket.Properties.VariableNames{'cheapBndPZero2Y'} = 'cheapBndPZero';
Basket.Properties.VariableNames{'cheapBndPBdt2Y'} = 'cheapBndPBdt';
Basket.Properties.VariableNames{'bndBdtYild2Y'} = 'bndBdtYild';
elseif type==5
Basket.Properties.VariableNames{'cheapBndPZero5Y'} = 'cheapBndPZero';
Basket.Properties.VariableNames{'cheapBndPBdt5Y'} = 'cheapBndPBdt';
Basket.Properties.VariableNames{'bndBdtYild5Y'} = 'bndBdtYild';
elseif type==10
Basket.Properties.VariableNames{'cheapBndPZero10Y'} = 'cheapBndPZero';
Basket.Properties.VariableNames{'cheapBndPBdt10Y'} = 'cheapBndPBdt';
Basket.Properties.VariableNames{'bndBdtYild10Y'} = 'bndBdtYild';
end