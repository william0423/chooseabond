%%  CUSIP
clc;
clear;
% In the part, we are going to input the whole bond info to our dataset.
% Code will be high in the final Matlab Report.
%delete 'dataArray' in clear statement in this CUSIP block
filename = '20170918outstanding1.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%s%s%f%{dd-MM-yy}D%s%s%s%s%s%s%f%f%f%f%f%f%{HH:mm:ss}D%{dd-MM-yy}D%{dd-MM-yy}D%f%s%s%{dd-MM-yy}D';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
clearvars filename delimiter startRow formatSpec fileID  ans;
%% Prepare data
% Firstly, the data will be calculated and add-on bond-type by its original
% maturity.
bond0= table(dataArray{1:end}, 'VariableNames', {'Cusip','VarName2','Coupon', ...
    'MatDat','Ccy','VarName6','Bid','Ask','NetChng','Type','BYield','AYield', ...
    'YldNetC','BmkSprd','SprdtoSW','ASWSprd','Time','Date','IssDate','AccInt', ...
    'CleanPrice','DirtyPrice','SetDate'});
%convert str number to number format
bond0.Bid = cellfun(@str2num, bond0.Bid);
bond0.Ask = cellfun(@str2num, bond0.Ask);
bond0.NetChng = cellfun(@str2num, bond0.NetChng);
bond0.CleanPrice = cellfun(@str2num, bond0.CleanPrice);
bond0.DirtyPrice = cellfun(@str2num, bond0.DirtyPrice);

bond0.Coupon = bond0.Coupon(:)/100;

% bond type
bond1 = bond0(:, {'Cusip','IssDate','MatDat','SetDate','Coupon','AccInt', ...
    'BYield','AYield','CleanPrice','DirtyPrice','Bid','Ask','Date'});
todayDate = bond1.Date(1,1);
tTM = minus(bond1.MatDat(:), todayDate);
bond1.tTM = tTM;

bondType = bond1{:, 'MatDat'} - bond1{:, 'IssDate'};
bondType = round(years(bondType), 0);
%bondType = str2num(char(bondType));
%bondType = table(bondType);
bond1.bondType = bondType;
clearvars bondType dataArray bond0;
%% yieldCurve from zero rate
% we set benchmark as 2, 3, 5, 7, 10, 30Yr. then do bondselection based on
% the benchmark.
% then we generate zero rate curve via bootstrape method
bsm = datetime(2017,8,15);
bondSelect=bondselection(bsm,bond1); %do bondselection
[zeroDates1 zeroRates settle]=bootstrapyldcv(bondSelect); %generate zero rate via bootstrape

%% price bond via zero rate
% based on zero rate, we compute the bond's fair value via bondbyzero
% method, using the price to calculate the yield of bond and its difference
% with original yield.
%[bndprc bndyield yielddiff]=bndbyzero(bond1,settle,zeroDates1,zeroRates);
rateSpec = intenvset('ValuationDate', settle, 'StartDates',  ...
    (bond1.SetDate(1)),'EndDates', zeroDates1, 'Rates', zeroRates, ...
    'Compounding',2,'Basis',0,'EndMonthRule',1);
bndzeroprc = bondbyzero(rateSpec, bond1.Coupon, settle, bond1.MatDat);
bndzeroYield = arrayfun(@(a,b,c,d) bndyield(a,b,c,d),bndzeroprc, bond1.Coupon, bond1.SetDate, bond1.MatDat); %makes yield curve
yielzeroddiff = bond1.BYield(:)/100 - bndzeroYield(:); %compare the yield with Byield
bond1=[table(bndzeroprc) table(bndzeroYield) table(yielzeroddiff) bond1];
%% Generate baskets
% Category different Basket for 2Y 5Y 10Y based on Contract Specification on CMEGroup.com 

fdelivery = datetime(2017,9,1);
ldelivery = datetime(2017,10,4);

[Basket2Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,2);
[Basket5Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,5);
[Basket10Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,10);
%[Basket30Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,30);
%% Volatility term structure script from CME 
% volatility term struction is based the real time info, we need collect
% the data live in the future.
volTermDates2Y = ['09-21-2019';'09-26-2019';'10-27-2019';'11-24-2019';'12-20-2019';'02-25-2019'];
volTerm2Y = [0.775;0.675;0.65;0.6625;0.6896;0.7031]/100;
volTermDates5Y = ['09-20-2022';'09-27-2022';'10-26-2022';'11-24-2022';'12-21-2022';'02-25-2022'];
volTerm5Y = [2.4281;2.1094;2.1719;2.2313;2.2438;2.275]/100;
volTermDates10Y = ['09-20-2027';'09-26-2027';'10-26-2027';'11-23-2027';'12-20-2027';'02-24-2027'];
volTerm10Y = [3.5187;3.0438;3.5938;3.75;4.0438;5.6563]/100;
%volTermDates30Y = ['09-20-2047';'09-26-2047';'10-26-2047';'11-24-2047';'12-21-2047';'02-24-2047'];
%volTerm30Y = [7.4333;6.4333;6.925;7.1625;7.1;7.0333]/100;

%% bond value by BDT
% compute the bond fair value via BDT method
[Basket2Y]=bndByBdt(Basket2Y,settle,volTermDates2Y,volTerm2Y,rateSpec);
[Basket5Y]=bndByBdt(Basket5Y,settle,volTermDates5Y,volTerm5Y,rateSpec);
[Basket10Y]=bndByBdt(Basket10Y,settle,volTermDates10Y,volTerm10Y,rateSpec);

%% bond bdt yield
% compute the yield via the BDT fair value, 
bndBdtYild2Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), Basket2Y.bndBdtPrc, Basket2Y.Coupon, Basket2Y.SetDate, Basket2Y.MatDat);    
bndBdtYild5Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), Basket5Y.bndBdtPrc, Basket5Y.Coupon, Basket5Y.SetDate, Basket5Y.MatDat); 
bndBdtYild10Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), Basket10Y.bndBdtPrc, Basket10Y.Coupon, Basket10Y.SetDate, Basket10Y.MatDat); 

%bondBdtYield30Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt30Y, bond30Y.Coupon, bond30Y.SetDate, bond30Y.MatDat); 
% compute the yield difference with original yield
bondBdtDiff2Y = Basket2Y.BYield(:)/100 - bndBdtYild2Y(:);
bondBdtDiff5Y = Basket5Y.BYield(:)/100 - bndBdtYild5Y(:);
bondBdtDiff10Y = Basket10Y.BYield(:)/100 - bndBdtYild10Y(:);
%bondBdtDiff30Y = Basket30Y.BYield(:)/100 - bondBdtYield30Y(:);
Basket2Y=[table(bndBdtYild2Y) table(bondBdtDiff2Y) Basket2Y];
Basket5Y=[table(bndBdtYild5Y) table(bondBdtDiff5Y) Basket5Y];
Basket10Y=[table(bndBdtYild10Y) table(bondBdtDiff10Y) Basket10Y];
%% draw a picture
magic(Basket2Y,Basket5Y,Basket10Y)

%% Cheapest to deliver bond by zero
% Using the bond by zero fair value divid conversion factor, we could
% compute the expected fair value of bond in the future
cheapBndPZero2Y=Basket2Y.bndzeroprc./Basket2Y.convFactor;
cheapBndPZero5Y=Basket5Y.bndzeroprc./Basket5Y.convFactor;
cheapBndPZero10Y=Basket10Y.bndzeroprc./Basket10Y.convFactor;

%% Cheapest to deliver bond bdt
% Using the BDT fair value divid conversion factor, we could
% compute the expected fair value of bond in the future
cheapBndPBdt2Y=Basket2Y.bndBdtPrc./Basket2Y.convFactor;
Basket2Y=[table(cheapBndPZero2Y) table(cheapBndPBdt2Y) Basket2Y];
cheapBndPBdt5Y=Basket5Y.bndBdtPrc./Basket5Y.convFactor;
Basket5Y=[table(cheapBndPZero5Y) table(cheapBndPBdt5Y) Basket5Y];
cheapBndPBdt10Y=Basket10Y.bndBdtPrc./Basket10Y.convFactor;
Basket10Y=[table(cheapBndPZero10Y) table(cheapBndPBdt10Y) Basket10Y];
%% change the table variable name
Basket2Y=changeAname(Basket2Y,2);
Basket5Y=changeAname(Basket5Y,5);
Basket10Y=changeAname(Basket10Y,10);
%% gross basis
Basket2Y=grosBsis(108.1641,Basket2Y);
Basket5Y=grosBsis(118.2891,Basket5Y);
Basket10Y=grosBsis(126.375,Basket10Y);
%% make a table
% in this section, we find the proper bond based minimum the {BondbyZero 
% Price/Conversion Factor}, {BondBbyBDT Price/Conversion Factor},
% Min{Implied Repo Rate}, the smallest one stands for the bond we are going
% to choose for delivery.
res2Y=[(Basket2Y.Cusip(find(Basket2Y.cheapBndPBdt==min(Basket2Y.cheapBndPBdt))))  ...
    (Basket2Y.Cusip(find(Basket2Y.cheapBndPZero==min(Basket2Y.cheapBndPZero))))  ...
    (Basket2Y.Cusip(find(Basket2Y.ImpRepo==min(Basket2Y.ImpRepo))))];

res5Y=[(Basket5Y.Cusip(find(Basket5Y.cheapBndPBdt==min(Basket5Y.cheapBndPBdt))))  ...
    (Basket5Y.Cusip(find(Basket5Y.cheapBndPZero==min(Basket5Y.cheapBndPZero))))  ...
    (Basket5Y.Cusip(find(Basket5Y.ImpRepo==min(Basket5Y.ImpRepo))))];

res10Y=[(Basket10Y.Cusip(find(Basket10Y.cheapBndPBdt==min(Basket10Y.cheapBndPBdt))))  ...
    (Basket10Y.Cusip(find(Basket10Y.cheapBndPZero==min(Basket10Y.cheapBndPZero))))  ...
    (Basket10Y.Cusip(find(Basket10Y.ImpRepo==min(Basket10Y.ImpRepo))))];
chooseBnd=[res2Y;res5Y;res10Y]
Res2Ybond= Basket2Y(:, {'cheapBndPBdt','cheapBndPZero','ImpRepo','Cusip'})
Res5Ybond= Basket5Y(:, {'cheapBndPBdt','cheapBndPZero','ImpRepo','Cusip'})
Res10Ybond= Basket10Y(:, {'cheapBndPBdt','cheapBndPZero','ImpRepo','Cusip'})
clearvars -except Basket2Y Basket5Y Basket10Y bond1 chooseBnd Res2Ybond Res5Ybond Res10Ybond