%change working directory of files
clc;
clear;

%%  CUSIP
%change working directory
%delete 'dataArray' in clear statement in this CUSIP block
filename = '20170918outstanding1.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%s%s%f%{dd-MM-yy}D%s%s%s%s%s%s%f%f%f%f%f%f%{HH:mm:ss}D%{dd-MM-yy}D%{dd-MM-yy}D%f%s%s%{dd-MM-yy}D';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
clearvars filename delimiter startRow formatSpec fileID  ans;
%% Prepare data
bond0= table(dataArray{1:end}, 'VariableNames', {'Cusip','VarName2','Coupon', ...
    'MatDat','Ccy','VarName6','Bid','Ask','NetChng','Type','BYield','AYield', ...
    'YldNetC','BmkSprd','SprdtoSW','ASWSprd','Time','Date','IssDate','AccInt', ...
    'CleanPrice','DirtyPrice','SetDate'});
%convert str number to number format
bond0.Bid = cellfun(@str2num, bond0.Bid);
bond0.Ask = cellfun(@str2num, bond0.Ask);
bond0.NetChng = cellfun(@str2num, bond0.NetChng);
bond0.CleanPrice = cellfun(@str2num, bond0.CleanPrice);
bond0.DirtyPrice = cellfun(@str2num, bond0.DirtyPrice);

bond0.Coupon = bond0.Coupon(:)/100;

% bond type
bond1 = bond0(:, {'Cusip','IssDate','MatDat','SetDate','Coupon','AccInt', ...
    'BYield','AYield','CleanPrice','DirtyPrice','Bid','Ask','Date'});
todayDate = bond1.Date(1,1);
tTM = minus(bond1.MatDat(:), todayDate);
bond1.tTM = tTM;

bondType = bond1{:, 'MatDat'} - bond1{:, 'IssDate'};
bondType = round(years(bondType), 0);
%bondType = str2num(char(bondType));
%bondType = table(bondType);
bond1.bondType = bondType;
clearvars bondType dataArray bond0;
%% Conversion factor
%all bond using first deliverable date

referDate = datetime(2017,9,1);
%bond1C = bond1(bond1.MatDat > referDate,:);
convFactor = convfactor(referDate, bond1.MatDat, bond1.Coupon);
bond1 = [table(convFactor) bond1];



clear bond1C convFactor
%% Generate baskets
%Contract Specification on CMEGroup.com determines min, max dates in the
%basket
%contractSpec [<, >, <]


fdelivery = datetime(2017,9,1);
ldelivery = datetime(2017,10,4);

[Basket2Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,2);
[Basket5Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,5);
[Basket10Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,10);
[Basket30Y,bond1]=generatebasket(bond1,fdelivery,ldelivery,30);
%% yieldCurve
%benchmark: 2, 3, 5, 7, 10, 30Yr bonds
bsm = datetime(2017,8,15);
%bondSelect=bondselection(bsm,bond1);
M = bsm.Month;
cuYr = bsm.Year;

benchmark = [2 3 5 7 10 30];
%bondSelect = cell2table(cell(13,width(bond1)));
%%Should use June contract for 10Y and 30Y, but couldn't find it
MSeq = [repelem(M, 6)];
bondSelect = bond1((bond1.MatDat.Year==cuYr+benchmark(1)) & (bond1.IssDate.Month == MSeq(1))&(bond1.MatDat.Month == MSeq(1))&(bond1.IssDate.Year==cuYr),:);
for i = 2:(length(benchmark))
    bondSelectNew = bond1((bond1.MatDat.Year==cuYr+benchmark(i)) & (bond1.IssDate.Month == MSeq(i)) &(bond1.MatDat.Month == MSeq(1))& (bond1.IssDate.Year==cuYr),:);
    bondSelect  = [bondSelect; bondSelectNew];
end
clear bsm benchmark
bondSelect
%% bondSelect
%use these to interpolate yield curve for yr in derived = []
derived = [4 6 8 9 15 20 25];
newBenchMark = [5 7 10 10 30 30 30];
newMSeq = [repelem(M, 7)];
for i=1:(length(derived))
    if isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.MatDat.Month == newMSeq(i))&(bond1.bondType == newBenchMark(i)),:)) == 0
    %most strict criteria, maturity month equal M  
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.MatDat.Month == newMSeq(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    elseif isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.bondType == newBenchMark(i)),:))  == 0
    %less strict criteria, maturity month not has to equal to issue month
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    elseif isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.bondType == newBenchMark(i)),:)) == 0
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    else 
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i)-1)&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    end
end

bondSelect = sortrows(bondSelect, 'tTM');
bondSelect
clear derived newBenchMark newMSeq bondSelectNew
%% bootstrap yield curve
bondInput = [datenum(bondSelect.SetDate) datenum(bondSelect.MatDat) bondSelect.CleanPrice];
InstrumentType = {'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond'};
settle = datenum(bondInput(1,1));
CouponRate = bondSelect.Coupon;
option = IRBootstrapOptions('LowerBound', -1);
CurveModel = IRDataCurve.bootstrap('zero', settle, InstrumentType, bondInput, 'InstrumentCouponRate', CouponRate, 'IRBootstrapOptions', option);


zeroDates = busdays(bondSelect.SetDate(1), bondSelect.MatDat(end));
%zeroDates = busdays(datenum(bondSelect.SetDate(1)), datenum(bondSelect.MatDat(end)));
zeroDates1 = zeroDates(2:end);
zeroRates = CurveModel.getZeroRates(zeroDates1);

%plot a zero rate curve
plot(zeroRates);
title('zero rate curve');
xlabel('Time/year');
ylabel('zero rate curve');
set(gca,'XtickLabel',{'0','4','8','12','16','20','24','28','32'});
saveas(gcf,'ZeroRateCurve.jpg')
clear bondInput InstrumentType CouponRate option CurveModel zeroDates
%% Bond value by zero curve
bond2Y = Basket2Y;
rateSpec = intenvset('ValuationDate', settle, 'StartDates', (bond1.SetDate(1)),'EndDates', zeroDates1, 'Rates', zeroRates, 'Compounding',2,'Basis',0,'EndMonthRule',1);
% bond 2Y
bondZero2Y = zeros(size(bond2Y,1),1);
for i = 1:size(bondZero2Y,1)
    bondZero2Y(i) = bondbyzero(rateSpec, bond2Y.Coupon(i), settle, bond2Y.MatDat(i));
end
% bond 5Y
bond5Y = Basket5Y;
bondZero5Y = zeros(size(bond5Y,1),1);
for i = 1:size(bondZero5Y,1)
    bondZero5Y(i) = bondbyzero(rateSpec, bond5Y.Coupon(i), settle, bond5Y.MatDat(i));
end
% bond 10Y
bond10Y = Basket10Y;
bondZero10Y = zeros(size(bond10Y,1),1);
for i = 1:size(bondZero10Y,1)
    bondZero10Y(i) = bondbyzero(rateSpec, bond10Y.Coupon(i), settle, bond10Y.MatDat(i));
end
% bond 30Y
% bondZero30Y = zeros(size(bond30Y,1),1);
% for i = 1:size(bondZero30Y,1)
%     bondZero30Y(i) = bondbyzero(rateSpec, bond30Y.Coupon(i), settle, bond30Y.MatDat(i));
% end
bond2Y=[table(bondZero2Y) bond2Y];
bond5Y=[table(bondZero5Y) bond5Y];
bond10Y=[table(bondZero10Y) bond10Y];

clear Basket2Y Basket5Y Basket10Y Basket30Y bondZero2Y bondZero5Y bondZero10Y
%% bond yield by zero curve
bondZeroYield2Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond2Y.bondZero2Y, bond2Y.Coupon, bond2Y.SetDate, bond2Y.MatDat);
bondZeroYield5Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond5Y.bondZero5Y, bond5Y.Coupon, bond5Y.SetDate, bond5Y.MatDat);
bondZeroYield10Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d),bond10Y.bondZero10Y, bond10Y.Coupon, bond10Y.SetDate, bond10Y.MatDat);
% bondZeroYield30Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondZero30Y, bond30Y.Coupon, bond30Y.SetDate, bond30Y.MatDat);
%% bond zero difference
bondZeroDiff2Y = bond2Y.BYield(:)/100 - bondZeroYield2Y(:);
bondZeroDiff5Y = bond5Y.BYield(:)/100 - bondZeroYield5Y(:);
bondZeroDiff10Y = bond10Y.BYield(:)/100 - bondZeroYield10Y(:);
% bondZeroDiff30Y = bond30Y.BYield(:)/100 - bondZeroYield30Y(:);


%% Volatility term structure script from CME 
% volSpec
volTermDates2Y = ['09-21-2019';'09-26-2019';'10-27-2019';'11-24-2019';'12-20-2019';'02-25-2019'];
volTerm2Y = [0.775;0.675;0.65;0.6625;0.6896;0.7031]/100;
volTermDates5Y = ['09-20-2022';'09-27-2022';'10-26-2022';'11-24-2022';'12-21-2022';'02-25-2022'];
volTerm5Y = [2.4281;2.1094;2.1719;2.2313;2.2438;2.275]/100;
volTermDates10Y = ['09-20-2027';'09-26-2027';'10-26-2027';'11-23-2027';'12-20-2027';'02-24-2027'];
volTerm10Y = [3.5187;3.0438;3.5938;3.75;4.0438;5.6563]/100;
volTermDates30Y = ['09-20-2047';'09-26-2047';'10-26-2047';'11-24-2047';'12-21-2047';'02-24-2047'];
volTerm30Y = [7.4333;6.4333;6.925;7.1625;7.1;7.0333]/100;

%% bond value by BDT
% rateSpec same as bondZeroPrice
% timeSpec created inside for loop
% valuation
valuationDate = settle;

% bond bdt 2y
volSpec = bdtvolspec(valuationDate, volTermDates2Y, volTerm2Y);
bondBdt2Y = zeros(size(bond2Y,1),1);
for i = 1:size(bondBdt2Y,1)
    Maturity = cfdates(bond2Y.SetDate(i), bond2Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bondBdt2Y(i) = bondbybdt(bdtTree, bond2Y.Coupon(i), settle, bond2Y.MatDat(i));
end
% bond bdt 5y
volSpec = bdtvolspec(valuationDate, volTermDates5Y, volTerm5Y);
bondBdt5Y = zeros(size(bond5Y,1),1);
for i = 1:size(bondBdt5Y,1)
    Maturity = cfdates(bond5Y.SetDate(i), bond5Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bondBdt5Y(i) = bondbybdt(bdtTree, bond5Y.Coupon(i), settle, bond5Y.MatDat(i));
end
% bond bdt 10y
volSpec = bdtvolspec(valuationDate, volTermDates10Y, volTerm10Y);
bondBdt10Y = zeros(size(bond10Y,1),1);
for i = 1:size(bondBdt10Y,1)
    Maturity = cfdates(bond10Y.SetDate(i), bond10Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bondBdt10Y(i) = bondbybdt(bdtTree, bond10Y.Coupon(i), settle, bond10Y.MatDat(i));
end

% bond bdt 30y
%volSpec = bdtvolspec(valuationDate, volTermDates30Y, volTerm30Y);
%bondBdt30Y = zeros(size(bond30Y,1),1);
%for i = 1:size(bondBdt30Y,1)
%    Maturity = cfdates(bond30Y.SetDate(i), bond30Y.MatDat(i), 2,0,1);
%    timeSpec = bdttimespec(valuationDate, Maturity);
%    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
%    bondBdt30Y(i) = bondbybdt(bdtTree, bond30Y.Coupon(i), settle, bond30Y.MatDat(i));
%end
bond2Y=[table(bondBdt2Y) bond2Y];
bond5Y=[table(bondBdt5Y) bond5Y];
bond10Y=[table(bondBdt10Y) bond10Y];

clear volSpec Maturity timeSpec bdtTree bondBdt2Y bondBdt5Y bondBdt10Y
%% bond bdt yield
bondBdtYield2Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond2Y.bondBdt2Y, bond2Y.Coupon, bond2Y.SetDate, bond2Y.MatDat);    
bondBdtYield5Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond5Y.bondBdt5Y, bond5Y.Coupon, bond5Y.SetDate, bond5Y.MatDat); 
bondBdtYield10Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond10Y.bondBdt10Y, bond10Y.Coupon, bond10Y.SetDate, bond10Y.MatDat); 

%bondBdtYield30Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt30Y, bond30Y.Coupon, bond30Y.SetDate, bond30Y.MatDat); 

bondBdtDiff2Y = bond2Y.BYield(:)/100 - bondBdtYield2Y(:);
bondBdtDiff5Y = bond5Y.BYield(:)/100 - bondBdtYield5Y(:);
bondBdtDiff10Y = bond10Y.BYield(:)/100 - bondBdtYield10Y(:);
%bondBdtDiff30Y = bond30Y.BYield(:)/100 - bondBdtYield30Y(:);

%% draw pic to compare the difference between yield
figure 
plot(bond2Y.BYield/100,'b')
hold on
plot(bondZeroYield2Y,'g--o')
hold on
plot(bondBdtYield2Y,'r*')
title('Comparing yield for 2Y bond');
xlabel('Time/year');
ylabel('yield');
legend('2Yoriginal yield','2Ybndbyzero yield','2Ybdt yield')
saveas(gcf,'ComparingYield2YBond.jpg')

figure 
plot(bond5Y.BYield/100,'b')
hold on
plot(bondZeroYield5Y,'g--o')
hold on
plot(bondBdtYield5Y,'r*')
title('Comparing yield for 5Y bond');
xlabel('Time/year');
ylabel('yield');
legend('5Yoriginal yield','5Ybndbyzero yield','5Ybdt yield')
saveas(gcf,'ComparingYield5YBond.jpg')

figure 
plot(bond10Y.BYield/100,'b')
hold on
plot(bondZeroYield10Y,'g--o')
hold on
plot(bondBdtYield10Y,'r*')
title('Comparing yield for 10Y bond');
xlabel('Time/year');
ylabel('yield');
legend('10Yoriginal yield','10Ybndbyzero yield','10Ybdt yield')
saveas(gcf,'ComparingYield10YBond.jpg')
%% Cheapest to deliver bond by zero
cheapBndPZero2Y=bond2Y.bondZero2Y./bond2Y.convFactor;
bond2Y=[table(cheapBndPZero2Y) bond2Y];

cheapBndPZero5Y=bond5Y.bondZero5Y./bond5Y.convFactor;
bond5Y=[table(cheapBndPZero5Y) bond5Y];

cheapBndPZero10Y=bond10Y.bondZero10Y./bond10Y.convFactor;
bond10Y=[table(cheapBndPZero10Y) bond10Y];
%% Cheapest to deliver bond bdt
cheapBndPBdt2Y=bond2Y.bondBdt2Y./bond2Y.convFactor;
bond2Y=[table(cheapBndPBdt2Y) bond2Y];

cheapBndPBdt5Y=bond5Y.bondBdt5Y./bond5Y.convFactor;
bond5Y=[table(cheapBndPBdt5Y) bond5Y];

cheapBndPBdt10Y=bond10Y.bondBdt10Y./bond10Y.convFactor;
bond10Y=[table(cheapBndPBdt10Y) bond10Y];

%% make a table
res2Y=[string(bond2Y.Cusip(find(min(bond2Y.cheapBndPBdt2Y))))  ...
    string(bond2Y.Cusip(find(min(bond2Y.cheapBndPZero2Y))))  ...
    string(bond2Y.Cusip(find(min(bond2Y.ImpRepo))))]

res5Y=[string(bond5Y.Cusip(find(min(bond5Y.cheapBndPBdt5Y))))  ...
    string(bond5Y.Cusip(find(min(bond5Y.cheapBndPZero5Y))))  ...
    string(bond5Y.Cusip(find(min(bond5Y.ImpRepo))))]

res10Y=[string(bond10Y.Cusip(find(min(bond10Y.cheapBndPBdt10Y))))  ...
    string(bond10Y.Cusip(find(min(bond10Y.cheapBndPZero10Y))))  ...
    string(bond10Y.Cusip(find(min(bond10Y.ImpRepo))))]

Res2Ybond= bond2Y(:, {'cheapBndPBdt2Y','cheapBndPZero2Y','ImpRepo','Cusip'})
Res5Ybond= bond5Y(:, {'cheapBndPBdt5Y','cheapBndPZero5Y','ImpRepo','Cusip'})
Res10Ybond= bond10Y(:, {'cheapBndPBdt10Y','cheapBndPZero10Y','ImpRepo','Cusip'})