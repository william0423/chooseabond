function [zeroDates1 zeroRates settle]=bootstrapyldcv(bondSelect)
bondInput = [datenum(bondSelect.SetDate) datenum(bondSelect.MatDat) bondSelect.CleanPrice];
InstrumentType = {'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond'};
settle = datenum(bondInput(1,1));
CouponRate = bondSelect.Coupon;
option = IRBootstrapOptions('LowerBound', -1);
CurveModel = IRDataCurve.bootstrap('zero', settle, InstrumentType, bondInput, 'InstrumentCouponRate', CouponRate, 'IRBootstrapOptions', option);


zeroDates = busdays(bondSelect.SetDate(1), bondSelect.MatDat(end));
%zeroDates = busdays(datenum(bondSelect.SetDate(1)), datenum(bondSelect.MatDat(end)));
zeroDates1 = zeroDates(2:end);
zeroRates = CurveModel.getZeroRates(zeroDates1);

%plot a zero rate curve
plot(zeroRates);
title('zero rate curve');
xlabel('Time/year');
ylabel('zero rate curve');
set(gca,'XtickLabel',{'0','4','8','12','16','20','24','28','32'});
saveas(gcf,'ZeroRateCurve.jpg')