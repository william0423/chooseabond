function bondSelect=bondselection(bsm,bond1)
M = bsm.Month;
cuYr = bsm.Year;

benchmark = [2 3 5 7 10 30];
%bondSelect = cell2table(cell(13,width(bond1)));
%%Should use June contract for 10Y and 30Y, but couldn't find it
MSeq = [repelem(M, 6)];
bondSelect = bond1((bond1.MatDat.Year==cuYr+benchmark(1)) & (bond1.IssDate.Month == MSeq(1))&(bond1.MatDat.Month == MSeq(1))&(bond1.IssDate.Year==cuYr),:);
for i = 2:(length(benchmark))
    bondSelectNew = bond1((bond1.MatDat.Year==cuYr+benchmark(i)) & (bond1.IssDate.Month == MSeq(i)) &(bond1.MatDat.Month == MSeq(1))& (bond1.IssDate.Year==cuYr),:);
    bondSelect  = [bondSelect; bondSelectNew];
end

derived = [4 6 8 9 15 20 25];
newBenchMark = [5 7 10 10 30 30 30];
newMSeq = [repelem(M, 7)];
for i=1:(length(derived))
    if isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.MatDat.Month == newMSeq(i))&(bond1.bondType == newBenchMark(i)),:)) == 0
    %most strict criteria, maturity month equal M  
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.MatDat.Month == newMSeq(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    elseif isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.bondType == newBenchMark(i)),:))  == 0
    %less strict criteria, maturity month not has to equal to issue month
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    elseif isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.bondType == newBenchMark(i)),:)) == 0
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    else 
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i)-1)&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    end
end

bondSelect = sortrows(bondSelect, 'tTM');