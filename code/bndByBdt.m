function[bond2Y]=bndByBdt(bond2Y,settle,volTermDates2Y,volTerm2Y,rateSpec)
valuationDate = settle;
volSpec = bdtvolspec(valuationDate, volTermDates2Y, volTerm2Y);
bondBdt2Y = zeros(size(bond2Y,1),1);
for i = 1:size(bondBdt2Y,1)
    Maturity = cfdates(bond2Y.SetDate(i), bond2Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bndBdtPrc(i) = bondbybdt(bdtTree, bond2Y.Coupon(i), settle, bond2Y.MatDat(i));
end
bndBdtPrc=bndBdtPrc';
bond2Y=[table(bndBdtPrc) bond2Y];