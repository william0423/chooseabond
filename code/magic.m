function magic(Basket2Y,Basket5Y,Basket10Y)

figure 
plot(Basket2Y.BYield/100,'b')
hold on
plot(Basket2Y.bndzeroYield,'g--o')
hold on
plot(Basket2Y.bndBdtYild2Y,'r*')
title('Comparing yield for 2Y bond');
xlabel('Time/year');
ylabel('yield');
legend('2Yoriginal yield','2Ybndbyzero yield','2Ybdt yield')
saveas(gcf,'ComparingYield2YBond.jpg')

figure 
plot(Basket5Y.BYield/100,'b')
hold on
plot(Basket5Y.bndzeroYield,'g--o')
hold on
plot(Basket5Y.bndBdtYild5Y,'r*')
title('Comparing yield for 5Y bond');
xlabel('Time/year');
ylabel('yield');
legend('5Yoriginal yield','5Ybndbyzero yield','5Ybdt yield')
saveas(gcf,'ComparingYield5YBond.jpg')

figure 
plot(Basket10Y.BYield/100,'b')
hold on
plot(Basket10Y.bndzeroYield,'g--o')
hold on
plot(Basket10Y.bndBdtYild10Y,'r*')
title('Comparing yield for 10Y bond');
xlabel('Time/year');
ylabel('yield');
legend('10Yoriginal yield','10Ybndbyzero yield','10Ybdt yield')
saveas(gcf,'ComparingYield10YBond.jpg')