%change working directory of files
clc;
clear;

%%  CUSIP
%change working directory
%delete 'dataArray' in clear statement in this CUSIP block
filename = '20170918outstanding1.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%s%s%f%{dd-MM-yy}D%s%s%s%s%s%s%f%f%f%f%f%f%{HH:mm:ss}D%{dd-MM-yy}D%{dd-MM-yy}D%f%s%s%{dd-MM-yy}D';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
clearvars filename delimiter startRow formatSpec fileID  ans;
%% Prepare data
bond0= table(dataArray{1:end}, 'VariableNames', {'Cusip','VarName2','Coupon','MatDat','Ccy','VarName6','Bid','Ask','NetChng','Type','BYield','AYield','YldNetC','BmkSprd','SprdtoSW','ASWSprd','Time','Date','IssDate','AccInt','CleanPrice','DirtyPrice','SetDate'});
%convert str number to number format
bond0.Bid = cellfun(@str2num, bond0.Bid);
bond0.Ask = cellfun(@str2num, bond0.Ask);
bond0.NetChng = cellfun(@str2num, bond0.NetChng);
bond0.CleanPrice = cellfun(@str2num, bond0.CleanPrice);
bond0.DirtyPrice = cellfun(@str2num, bond0.DirtyPrice);

bond0.Coupon = bond0.Coupon(:)/100;

clearvars dataArray;

%% bond type
bond1 = bond0(:, {'Cusip','IssDate','MatDat','SetDate','Coupon','AccInt','BYield','AYield','CleanPrice','DirtyPrice','Bid','Ask','Date'});

%date issue in original data: maturity date less than issue date due to
%wrong category of yy: (2030 or 1930)
%for i=1:size(bond1,1)
%    if bond1.MatDat(i) < bond1.IssDate(i)
%        bond1.MatDat(i) = bond1.MatDat(i).Year + years(100);
%    end
%end

%%
bondType = bond1{:, 'MatDat'} - bond1{:, 'IssDate'};

bondType = round(years(bondType), 0);
%bondType = str2num(char(bondType));
%bondType = table(bondType);
bond1.bondType = bondType;
%bond1(1:8, 1:8)

%% Generate baskets
%Contract Specification on CMEGroup.com determines min, max dates in the
%basket
%contractSpec [<, >, <]
todayDate = bond1.Date(1,1);
fdelivery = datetime(2017,12,1);
ldelivery = datetime(2017,12,31);

tTM = minus(bond1.MatDat(:), todayDate);
bond1.tTM = tTM;

BondTypeList = [2 5 7 3 10 30];
bond2Y = bond1(ismember(bond1.bondType,BondTypeList(1)), :);
bond5Y = bond1(ismember(bond1.bondType,BondTypeList(2)), :);
bond7Y = bond1(ismember(bond1.bondType,BondTypeList(3)), :);
bond3Y = bond1(ismember(bond1.bondType,BondTypeList(4)), :);
bond10Y = bond1(ismember(bond1.bondType,BondTypeList(5)), :);
bond30Y = bond1(ismember(bond1.bondType,BondTypeList(6)), :);

%Diff1 = minus(bond2Y.Maturity, today);
%Diff2 = minus(bond2Y.Maturity, delivery);
%Diff3 = minus(bond2Y.Maturity, delivery);
%Diff1.Format= 'y';
%Diff2.Format = 'y';
%Diff3.Format = 'y';



%ContractSpec2Y(1)
%% Generate basket
durationUb = minus(todayDate+calyears(5)+calmonths(3),todayDate);
durationMin = minus(fdelivery+(calyears(1)+calmonths(9)),fdelivery);
durationMax = minus(ldelivery+calyears(2), ldelivery);
Basket2Y = bond2Y((bond2Y.tTM <= durationUb)&(bond2Y.tTM>=durationMin)&(bond2Y.tTM<=durationMax),:);
%Basket2Y
%for i = 1:length(BondTypeList)
    %bondSubi = 
%end
%bond2Y = ;


%%
%display([bond2Y(end,1);bond3Y(end,1);bond5Y(end,1);bond7Y(end,1);bond10Y(end,1);bond30Y(end,1)])
%find(bond2Y{:, 'Maturity'} == min(bond2Y{:, 'Maturity'}))
%[minVal, minIndex] = min(bond2Y{:, 'Maturity'});

%bond2(20,:)


%%
%basket2@butterfly.Maturity = basket2@butterfly.Maturity+days(90);

%% bond yield

%
%Yield = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond1.CleanPrice, bond1.Coupon, bond1.SetDate, bond1.MatDat);
%bond1.Yield = Yield;
%% yieldCurve
%benchmark: 2, 3, 5, 7, 10, 30Yr bonds
bsm = datetime(2017,8,15);
M = bsm.Month;
cuYr = bsm.Year;

benchmark = [2 3 5 7 10 30];
%bondSelect = cell2table(cell(13,width(bond1)));
%%Should use June contract for 10Y and 30Y, but couldn't find it
MSeq = [repelem(M, 6)];
bondSelect = bond1((bond1.MatDat.Year==cuYr+benchmark(1)) & (bond1.IssDate.Month == MSeq(1))&(bond1.MatDat.Month == MSeq(1))&(bond1.IssDate.Year==cuYr),:);
for i = 2:(length(benchmark))
    bondSelectNew = bond1((bond1.MatDat.Year==cuYr+benchmark(i)) & (bond1.IssDate.Month == MSeq(i)) &(bond1.MatDat.Month == MSeq(1))& (bond1.IssDate.Year==cuYr),:);
    bondSelect  = [bondSelect; bondSelectNew];
end
bondSelect
%% bondSelect
%use these to interpolate yield curve for yr in derived = []
derived = [4 6 8 9 15 20 25];
newBenchMark = [5 7 10 10 30 30 30];
newMSeq = [repelem(M, 7)];
for i=1:(length(derived))
    if isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.MatDat.Month == newMSeq(i))&(bond1.bondType == newBenchMark(i)),:)) == 0
    %most strict criteria, maturity month equal M  
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.MatDat.Month == newMSeq(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    elseif isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.bondType == newBenchMark(i)),:))  == 0
    %less strict criteria, maturity month not has to equal to issue month
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.IssDate.Month==newMSeq(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    elseif isempty(bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.bondType == newBenchMark(i)),:)) == 0
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i))&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    else 
        bondSelectNew = bond1((bond1.MatDat.Year==cuYr+derived(i)-1)&(bond1.bondType == newBenchMark(i)),:);
        bondSelect = [bondSelect; bondSelectNew];
    %else 
    %    for j = -1:(-5)
    %        if isempty(bond1((bond1.MatDat.Year==cuYr+derived(i)-)&(bond1.bondType == newBenchMark(i)),:)) == 0
            
    %    bondSelectNew = 
    end
end

%use August as maturity month for the rest, couldn't find 10 year and 30
%year futures contract that mature in July
%for i = 3:4
%    bondSelectNew = bond1((bond1.Maturity.Year==cuYr+derived(i)) & (bond1.Maturity.Month == datetime(2017,8,15).Month) & (bond1.bondType == newBenchMark(i)),:);
%    bondSelect = [bondSelect; bondSelectNew];
%end

bondSelect = sortrows(bondSelect, 'tTM');
bondSelect

%% bootstrap yield curve
bondInput = [datenum(bondSelect.SetDate) datenum(bondSelect.MatDat) bondSelect.CleanPrice];
InstrumentType = {'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond';'bond'};
settle = datenum(bondInput(1,1));
CouponRate = bondSelect.Coupon;
option = IRBootstrapOptions('LowerBound', -1);
CurveModel = IRDataCurve.bootstrap('zero', settle, InstrumentType, bondInput, 'InstrumentCouponRate', CouponRate, 'IRBootstrapOptions', option);


zeroDates = busdays(bondSelect.SetDate(1), bondSelect.MatDat(end));
zeroDates1 = zeroDates(2:end);
zeroRates = CurveModel.getZeroRates(zeroDates1);

%% Conversion factor
%all bond using first deliverable date

referDate = datetime(2017,12,1);
bond1C = bond1(bond1.MatDat > referDate,:);
for i = 1:size(bond1C, 1)
    convFactor(i) = convfactor(referDate, bond1C.MatDat(i), bond1C.Coupon(i));
end
convFactor = convFactor';
bond1C = [table(convFactor) bond1C];


%% Bond value by zero curve
rateSpec = intenvset('ValuationDate', settle, 'StartDates', bond1.SetDate(1),'EndDates', zeroDates1, 'Rates', zeroRates, 'Compounding',2,'Basis',0,'EndMonthRule',1);
% bond 2Y
bondZero2Y = zeros(size(bond2Y,1),1);
for i = 1:size(bondZero2Y,1)
    bondZero2Y(i) = bondbyzero(rateSpec, bond2Y.Coupon(i), settle, bond2Y.MatDat(i));
end
% bond 5Y
bondZero5Y = zeros(size(bond5Y,1),1);
for i = 1:size(bondZero5Y,1)
    bondZero5Y(i) = bondbyzero(rateSpec, bond5Y.Coupon(i), settle, bond5Y.MatDat(i));
end
% bond 10Y
bondZero10Y = zeros(size(bond10Y,1),1);
for i = 1:size(bondZero10Y,1)
    bondZero10Y(i) = bondbyzero(rateSpec, bond10Y.Coupon(i), settle, bond10Y.MatDat(i));
end
% bond 30Y
bondZero30Y = zeros(size(bond30Y,1),1);
for i = 1:size(bondZero30Y,1)
    bondZero30Y(i) = bondbyzero(rateSpec, bond30Y.Coupon(i), settle, bond30Y.MatDat(i));
end

%% bond yield by zero curve
bondZeroYield2Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondZero2Y, bond2Y.Coupon, bond2Y.SetDate, bond2Y.MatDat);
bondZeroYield5Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondZero5Y, bond5Y.Coupon, bond5Y.SetDate, bond5Y.MatDat);
bondZeroYield10Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondZero10Y, bond10Y.Coupon, bond10Y.SetDate, bond10Y.MatDat);
bondZeroYield30Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondZero30Y, bond30Y.Coupon, bond30Y.SetDate, bond30Y.MatDat);
%% bond zero difference
bondZeroDiff2Y = bond2Y.BYield(:)/100 - bondZeroYield2Y(:);
bondZeroDiff5Y = bond5Y.BYield(:)/100 - bondZeroYield5Y(:);
bondZeroDiff10Y = bond10Y.BYield(:)/100 - bondZeroYield10Y(:);
bondZeroDiff30Y = bond30Y.BYield(:)/100 - bondZeroYield30Y(:);

%% Volatility term structure script from CME 
% volSpec
volTermDates2Y = ['09-21-2019';'09-26-2019';'10-27-2019';'11-24-2019';'12-20-2019';'02-25-2019'];
volTerm2Y = [0.775;0.675;0.65;0.6625;0.6896;0.7031]/100;
volTermDates5Y = ['09-20-2022';'09-27-2022';'10-26-2022';'11-24-2022';'12-21-2022';'02-25-2022'];
volTerm5Y = [2.4281;2.1094;2.1719;2.2313;2.2438;2.275]/100;
volTermDates10Y = ['09-20-2027';'09-26-2027';'10-26-2027';'11-23-2027';'12-20-2027';'02-24-2027'];
volTerm10Y = [3.5187;3.0438;3.5938;3.75;4.0438;5.6563]/100;
volTermDates30Y = ['09-20-2047';'09-26-2047';'10-26-2047';'11-24-2047';'12-21-2047';'02-24-2047'];
volTerm30Y = [7.4333;6.4333;6.925;7.1625;7.1;7.0333]/100;

%% bond value by BDT
% rateSpec same as bondZeroPrice
% timeSpec created inside for loop
% valuation
valuationDate = settle;

% bond bdt 2y
volSpec = bdtvolspec(valuationDate, volTermDates2Y, volTerm2Y);
bondBdt2Y = zeros(size(bond2Y,1),1);
for i = 1:size(bondBdt2Y,1)
    Maturity = cfdates(bond2Y.SetDate(i), bond2Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bondBdt2Y(i) = bondbybdt(bdtTree, bond2Y.Coupon(i), settle, bond2Y.MatDat(i));
end
%% bond bdt 5y
volSpec = bdtvolspec(valuationDate, volTermDates5Y, volTerm5Y);
bondBdt5Y = zeros(size(bond5Y,1),1);
for i = 1:size(bondBdt5Y,1)
    Maturity = cfdates(bond5Y.SetDate(i), bond5Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bondBdt5Y(i) = bondbybdt(bdtTree, bond5Y.Coupon(i), settle, bond5Y.MatDat(i));
end
%% bond bdt 10y
volSpec = bdtvolspec(valuationDate, volTermDates10Y, volTerm10Y);
bondBdt10Y = zeros(size(bond10Y,1),1);
for i = 1:size(bondBdt10Y,1)
    Maturity = cfdates(bond10Y.SetDate(i), bond10Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
    bondBdt10Y(i) = bondbybdt(bdtTree, bond10Y.Coupon(i), settle, bond10Y.MatDat(i));
end

%% bond bdt 30y
volSpec = bdtvolspec(valuationDate, volTermDates30Y, volTerm30Y);
bondBdt30Y = zeros(size(bond30Y,1),1);
for i = 1:size(bondBdt30Y,1)
    Maturity = cfdates(bond30Y.SetDate(i), bond30Y.MatDat(i), 2,0,1);
    timeSpec = bdttimespec(valuationDate, Maturity);
    bdtTree = bdttree(volSpec, rateSpec, timeSpec);
   % bondBdt30Y(i) = bondbybdt(bdtTree, bond30Y.Coupon(i), settle, bond30Y.MatDat(i));
end

%% bond bdt yield
bondBdtYield2Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt2Y, bond2Y.Coupon, bond2Y.SetDate, bond2Y.MatDat);    
bondBdtYield5Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt5Y, bond5Y.Coupon, bond5Y.SetDate, bond5Y.MatDat); 
bondBdtYield10Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt10Y, bond10Y.Coupon, bond10Y.SetDate, bond10Y.MatDat); 
%%
%bondBdtYield30Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt30Y, bond30Y.Coupon, bond30Y.SetDate, bond30Y.MatDat); 
%%
bondBdtDiff2Y = bond2Y.BYield(:)/100 - bondBdtYield2Y(:);
bondBdtDiff5Y = bond5Y.BYield(:)/100 - bondBdtYield5Y(:);
bondBdtDiff10Y = bond10Y.BYield(:)/100 - bondBdtYield10Y(:);
%%
%bondBdtDiff30Y = bond30Y.BYield(:)/100 - bondBdtYield30Y(:);

% %% Bond value by HW
% % volSpec
% volTermDates3m = ['11-26-2019';'11-24-2022';'11-23-2027';'11-24-2047'];
% volTerm3m = [0.6625; 2.2063; 3.75; 7.1625]/100;
% valuationDate = settle;
% volSpec = hwvolspec(valuationDate, volTermDates3m, volTerm3m);
% % rateSpec same as bondZeroPrice
% % timeSpec created inside for loop
% % valuation
% bondBdt = zeros(size(bond1,1),1);
% 
% for i = 1:size(bond1,1)
%     Maturity = cfdates(bond1.SetDate(i), bond1.MatDat(i), 2,0,1);
%     timeSpec = bdttimespec(valuationDate, Maturity);
%     bdtTree = bdttree(volSpec, rateSpec, timeSpec);
%     bondBdt(i) = bondbybdt(bdtTree, bond1.Coupon(i), settle, bond1.MatDat(i));
% end
% 
% bondBdt;
%  
% bondBdtYield = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondBdt, bond1.Coupon, bond1.SetDate, bond1.MatDat);    
% bondBdtDiff = bond1.BYield(:)/100 - bondBdtYield(:);



% volSpec = bdtvolspec(today, 

%% output csv
% za = zeros(size(ZeroRates,1), 4);
% for i = 1:size(ZeroRates,1)
%     za(i,1) = ;
%     za(i,2) = zM(i);
%     za(i,3) = zD(i);
%     za(i,4) = ZeroRates(i);
% end
% 
% csvwrite('zeroRates.csv', za)



