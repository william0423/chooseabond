function Basket=grosBsis(futprc,Basket)
% to compute the gross basis
grossZro=Basket.bndzeroprc-Basket.convFactor*futprc;
grossBdt=Basket.bndBdtPrc-Basket.convFactor*futprc;
Basket=[table(grossZro) table(grossBdt) Basket];
