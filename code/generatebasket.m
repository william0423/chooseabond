function [Basket,bond1]=generatebasket(bond1,fdelivery,ldelivery,type)
todayDate = bond1.Date(1,1);
%fdelivery = datetime(2017,9,1);
%ldelivery = datetime(2017,10,4);
%tTM = minus(bond1.MatDat(:), todayDate);
diff1=minus(todayDate,fdelivery);
diff2=minus(todayDate,ldelivery);
%bond1.tTM = tTM;
if type==2
durationMin = minus(fdelivery+(calyears(1)+calmonths(9))-diff1,fdelivery);
durationMax = minus(ldelivery+calyears(2)-diff2, ldelivery);
Basket = bond1((bond1.bondType <= 5)&(bond1.tTM>=(durationMin))&(bond1.tTM<=durationMax),:);  %%%
convFactor=convfactor(datetime(2017,10,4), Basket.MatDat, Basket.Coupon);
Basket=[table(convFactor) Basket];
ImpRepo = bndfutimprepo(Basket.Bid,108.1641,'10/4/2017','9/1/2017',Basket.convFactor,Basket.Coupon,Basket.MatDat);
Basket=[table(ImpRepo) Basket];

elseif type==5
%% Generate basket 5 year
durationUb5 = minus(todayDate+calyears(5)+calmonths(3),todayDate);
durationMin5 = minus(fdelivery+(calyears(4)+calmonths(2))-diff1,fdelivery);
%durationMax5 = minus(ldelivery+calyears(2), ldelivery);
Basket = bond1((bond1.bondType < 6)&(bond1.tTM>=(durationMin5)),:);  %%%
convFactor=convfactor(datetime(2017,10,4), Basket.MatDat, Basket.Coupon);
Basket=[table(convFactor) Basket];
ImpRepo = bndfutimprepo(Basket.Bid,118.2891,'10/4/2017','9/1/2017',Basket.convFactor,Basket.Coupon,Basket.MatDat);
Basket=[table(ImpRepo) Basket];

elseif type==10
%% Generate basket 10 year
durationMin10 = minus(fdelivery+(calyears(6)+calmonths(9))-diff1,fdelivery);
durationMax10 = minus(ldelivery+calyears(10)+calmonths(0)-diff1, ldelivery);
Basket = bond1((bond1.tTM>=(durationMin10))&(bond1.tTM<=durationMax10)&(bond1.bondType~=30),:);  %%%
convFactor=convfactor(datetime(2017,9,29), Basket.MatDat, Basket.Coupon);
Basket=[table(convFactor) Basket];
ImpRepo = bndfutimprepo(Basket.Bid,126.375,'10/4/2017','9/1/2017',Basket.convFactor,Basket.Coupon,Basket.MatDat);
Basket=[table(ImpRepo) Basket];

%% generate basket for 30 year
elseif type==30
durationMin30 = minus(fdelivery+(calyears(15))-diff1,fdelivery);
durationMax30 = minus(ldelivery+calyears(25)-diff1, ldelivery);
Basket = bond1((bond1.tTM>=(durationMin30))&(bond1.tTM<=durationMax30),:);     
end
