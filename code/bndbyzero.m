function [bndprc bndyield yielddiff]=bndbyzero(bond1,settle,zeroDates1,zeroRates)
%% Bond value by zero curve
%bond2Y = Basket2Y;
rateSpec = intenvset('ValuationDate', settle, 'StartDates',  ...
    (bond1.SetDate(1)),'EndDates', zeroDates1, 'Rates', zeroRates, ...
    'Compounding',2,'Basis',0,'EndMonthRule',1);
bndprc = bondbyzero(rateSpec, bond1.Coupon, settle, bond1.MatDat);
bndYield = arrayfun(@(a,b,c,d) bndyield(a,b,c,d),bndprc, bond1.Coupon, bond1.SetDate, bond1.MatDat);
yielddiff = bond1.BYield(:)/100 - bndYield(:);
%{ 
bond 2Y
bondZero2Y = zeros(size(bond2Y,1),1);
for i = 1:size(bondZero2Y,1)
    bondZero2Y(i) = bondbyzero(rateSpec, bond2Y.Coupon(i), settle, bond2Y.MatDat(i));
end
% bond 5Y
bond5Y = Basket5Y;
bondZero5Y = zeros(size(bond5Y,1),1);
for i = 1:size(bondZero5Y,1)
    bondZero5Y(i) = bondbyzero(rateSpec, bond5Y.Coupon(i), settle, bond5Y.MatDat(i));
end
% bond 10Y
bond10Y = Basket10Y;
bondZero10Y = zeros(size(bond10Y,1),1);
for i = 1:size(bondZero10Y,1)
    bondZero10Y(i) = bondbyzero(rateSpec, bond10Y.Coupon(i), settle, bond10Y.MatDat(i));
end
% bond 30Y
% bondZero30Y = zeros(size(bond30Y,1),1);
% for i = 1:size(bondZero30Y,1)
%     bondZero30Y(i) = bondbyzero(rateSpec, bond30Y.Coupon(i), settle, bond30Y.MatDat(i));
% end
bond2Y=[table(bondZero2Y) bond2Y];
bond5Y=[table(bondZero5Y) bond5Y];
bond10Y=[table(bondZero10Y) bond10Y];

clear Basket2Y Basket5Y Basket10Y Basket30Y bondZero2Y bondZero5Y bondZero10Y

%% bond yield by zero curve
bondZeroYield2Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond2Y.bondZero2Y, bond2Y.Coupon, bond2Y.SetDate, bond2Y.MatDat);
bondZeroYield5Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bond5Y.bondZero5Y, bond5Y.Coupon, bond5Y.SetDate, bond5Y.MatDat);
bondZeroYield10Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d),bond10Y.bondZero10Y, bond10Y.Coupon, bond10Y.SetDate, bond10Y.MatDat);
% bondZeroYield30Y = arrayfun(@(a,b,c,d) bndyield(a,b,c,d), bondZero30Y, bond30Y.Coupon, bond30Y.SetDate, bond30Y.MatDat);
%% bond zero difference
bondZeroDiff2Y = bond2Y.BYield(:)/100 - bondZeroYield2Y(:);
bondZeroDiff5Y = bond5Y.BYield(:)/100 - bondZeroYield5Y(:);
bondZeroDiff10Y = bond10Y.BYield(:)/100 - bondZeroYield10Y(:);
% bondZeroDiff30Y = bond30Y.BYield(:)/100 - bondZeroYield30Y(:);
%}
